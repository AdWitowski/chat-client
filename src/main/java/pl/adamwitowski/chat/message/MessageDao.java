package pl.adamwitowski.chat.message;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class MessageDao {

	@PersistenceContext
	EntityManager em;

	@Value("${auth.username}")
	private String authenticatedUser;

	public void create(Message message) {
		em.persist(message);
	}
	
	public void update(Message message){
		em.merge(message);
	}

	public void delete(Integer id) {
		Message messageToDelete = em.find(Message.class, id);
		em.remove(messageToDelete);
	}
	
	public void deleteAll(String username) {
		em.createQuery("DELETE FROM Message m WHERE m.receiverId = ? OR m.senderId = ?").setParameter(1, username).setParameter(2, username).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Message> getAllMessagesFromUsername(String username) {
		return em.createNativeQuery("SELECT * FROM MESSAGE WHERE RECEIVER_ID = ? OR SENDER_ID = ?;", Message.class)
				.setParameter(1, username).setParameter(2, username).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Message> getAllNewMessagesFromUsername(String username) {
		return em.createNativeQuery(
				"SELECT * FROM MESSAGE WHERE RECEIVER_ID = ? AND SENDER_ID = ? AND NEW_MESSAGE = 'T';", Message.class)
				.setParameter(1, authenticatedUser).setParameter(2, username).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllUsersWithNewMessages() {
		return em.createNativeQuery("SELECT SENDER_ID FROM MESSAGE WHERE RECEIVER_ID = ? AND NEW_MESSAGE = 'T';",
				Message.class).setParameter(1, authenticatedUser).getResultList();

	}
	

	public Message getLastMessagesFromUsername(String username){
		TypedQuery<Message> query = em.createQuery("SELECT m FROM Message m WHERE m.receiverId = ? OR m.senderId = ? ORDER BY m.created DESC",
				Message.class).setParameter(1, username).setParameter(2, username);
		List<Message> result = query.getResultList();
		
		if(result.isEmpty()){
			
			Message message = new Message();
			message.setBody("No messages");
			return message;
		}
		else{
			return result.get(0);
		}
		
	}

	public void changeNewMessageColumnValue(Integer id) {
		Query update = em.createQuery("UPDATE Message m SET m.newMessage='false' WHERE m.id = ?").setParameter(1, id);
		update.executeUpdate();
	}
}
